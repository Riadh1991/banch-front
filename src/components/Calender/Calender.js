import React from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment' 
import 'react-big-calendar/lib/css/react-big-calendar.css'

const localizer = momentLocalizer(moment)

const MyCalendar = props => {
    return (
      <div style={{height: '80vh', margin: '10px'}}>
        <Calendar
            localizer={localizer}
            events={[]}
            startAccessor="start"
            endAccessor="end"
            min-height = "500px"
        />
      </div>
    
    )

}


export default MyCalendar