import React from 'react';
import Logo from '../../../assets/images/oui-sncf.svg';
import classes from './Logo.module.css';



const logo = props => (
    <div className={classes['Logo']}>
        <img src={Logo} alt='Davos Logo' height='100px' width='100px'/>
    </div>);


export default logo;
