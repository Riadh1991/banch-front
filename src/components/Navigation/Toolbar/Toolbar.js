import React from 'react';
import Logo from '../../UI/Logo/Logo';
import classes from './Toolbar.module.css';
// import NavigationItems from '../NavigationItems/NavigationItems';


const toolbar = props => (
    <header className={classes['Toolbar']}>
            <div className={classes['Logo']}>
                <Logo />
            </div> 
                {/* <NavigationItems/> */}
    </header>
);

export default toolbar;
