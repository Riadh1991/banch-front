import React, { Component } from 'react';
import classes from './App.module.css';
import Layout from './HOC/Layout/Layout';

class App extends Component {
  render() {
    return (
      <div className={classes['App']}>
      <Layout>
        {/* <Switch>
          <Route path='/wizard'  component={WizardMain} />
          <Route path='/' exact component={underConstruction} />
        </Switch> */}
      </Layout>
    </div>

  );
  }
}

export default App;
