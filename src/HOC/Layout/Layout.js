import React, { Component, Fragment } from 'react';
import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
// import Footer from '../../components/Navigation/Footer/Footer';
import Calender from '../../components/Calender/Calender';
class Layout extends Component  {

    render() {

        return (
            <Fragment>
                    <Toolbar />
                    <main className={classes['Content']}>
                        {this.props.children}
                        INNO Banch Calender
                        <Calender />
                    </main>
                    {/* <Footer /> */}
            </Fragment>  
        );
    }
}

export default Layout;
